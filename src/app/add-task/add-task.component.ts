import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { TasksService } from '../services/tasks.services';
import { Task } from '../model/task';


@Component({
  selector: 'app-add-task',
  templateUrl: './add-task.component.html',
  styleUrls: ['./add-task.component.css']
})
export class AddTaskComponent implements OnInit {
  newTask: string;
  constructor(private taskService: TasksService) { }

  ngOnInit() {
  }

  addTask() {
    const task: Task = ({name: this.newTask, created: new Date(), isDone: false});
    this.taskService.addNewTask(task);
    this.newTask = '';
  }

}
