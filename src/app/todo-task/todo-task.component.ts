import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { TasksService } from '../services/tasks.services';
import { Task } from '../model/task';
import {CdkDragDrop, moveItemInArray} from '@angular/cdk/drag-drop';

@Component({
  selector: 'app-todo-task',
  templateUrl: './todo-task.component.html',
  styleUrls: ['./todo-task.component.css']
})
export class TodoTaskComponent implements OnInit {

  taskList: Task[] = [];

  constructor(private taskService: TasksService) {
    this.taskService.getTaskListObs().subscribe((tasks: Array<Task>) => {
      this.taskList = tasks;
    });
   }

  ngOnInit() {
  }

  removeTask(task: Task) {
    this.taskService.removeTask(task);
  }

  taskDone(task: Task) {
    this.taskService.taskDone(task);
  }

  showTaskInfo(i: number) {
    const el: HTMLElement = document.getElementById('task-' + i);
    const visibility = el.style.visibility;

    if (visibility === 'visible') {
      el.style.visibility = 'collapse';
    } else {
      el.style.visibility = 'visible';
    }
  }

  drop(event: CdkDragDrop<string[]>) {
    moveItemInArray(this.taskList, event.previousIndex, event.currentIndex);
  }

}
