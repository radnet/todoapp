import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { Task } from '../model/task';
import { NotificationService } from './notification.service';

@Injectable()
export class TasksService {

  private taskList: Task[] = [];
  private doneList: Task[] = [];

  private taskListObs = new BehaviorSubject<Array<Task>>([]);
  private doneListObs = new BehaviorSubject<Array<Task>>([]);

  constructor(private notifiService: NotificationService) {
    this.taskList = [
      { name: 'todo1', created: new Date(), isDone: false },
      { name: 'todo2', created: new Date(), isDone: false },
      { name: 'todo3', created: new Date(), isDone: false },
      { name: 'todo4', created: new Date(), isDone: false }
    ];
    this.taskListObs.next(this.taskList);
  }

  addNewTask(task: Task) {
    this.validateTask(task);

    task.isDone = false;
    this.taskList.push(task);
    this.taskListObs.next(this.taskList);
    console.log('Add new task: ' + task);
  }

  validateTask(task: Task) {
    if (!task.name) { this.notifiService.show('Name must be defined'); }
    task.name = task.name.trim();
    if (task.name.length < 1) { this.notifiService.show('Empty task'); }
  }

  removeTask(task: Task) {
    this.taskList = this.taskList.filter(e => e !== task);
    this.taskListObs.next(this.taskList);
    console.log('Remove task: ' + task);
  }

  taskDone(task: Task) {
    task.isDone = true;
    task.end = new Date();
    this.doneList.push(task);
    this.removeTask(task);
    this.doneListObs.next(this.doneList);
    console.log('Task done: ' + task);
  }

  removeTaskDone(task: Task) {
    this.doneList = this.doneList.filter(e => e !== task);
    this.doneListObs.next(this.doneList);

  }

  undoTask(task: Task) {
    this.removeTaskDone(task);
    this.addNewTask(task);
  }

  getTaskListObs(): Observable<Array<Task>> {
    return this.taskListObs.asObservable();
  }

  getDoneListObs(): Observable<Array<Task>> {
    return this.doneListObs.asObservable();
  }

}
