import { Injectable } from '@angular/core';

@Injectable()
export class NotificationService {

  constructor() {

  }

  show(msg: string) {
    window.alert(msg);
    throw Error(msg);
  }

}
