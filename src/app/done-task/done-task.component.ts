import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { TasksService } from '../services/tasks.services';
import { Task } from '../model/task';

@Component({
  selector: 'app-done-task',
  templateUrl: './done-task.component.html',
  styleUrls: ['./done-task.component.css']
})
export class DoneTaskComponent implements OnInit {

  doneList: Task[] = [];

  constructor(private taskService: TasksService) {
    this.taskService.getDoneListObs().subscribe((tasks: Array<Task>) => {
      this.doneList = tasks;
    });
   }

  ngOnInit() {
  }

  removeTaskDone(task: Task) {
    this.taskService.removeTaskDone(task);
  }

  undoTask(task: Task) {
    this.removeTaskDone(task);
    this.taskService.undoTask(task);
  }

  showTaskInfo(i: number) {
    const el: HTMLElement = document.getElementById('taskDone-' + i);
    const visibility = el.style.visibility;

    if (visibility === 'visible') {
      el.style.visibility = 'collapse';
    } else {
      el.style.visibility = 'visible';
    }
  }
}
