export interface Task {
  name: string;
  created: Date;
  isDone: boolean;
  end?: Date;
}
